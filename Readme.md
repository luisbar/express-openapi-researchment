This is an example of a Pet api by using [express-openapi](https://www.npmjs.com/package/express-openapi), which is an unopinionated OpenAPI framework for express.

## How to run?
- Install Voltajs
- Clone the repo
- Install dependencies with Yarn
- Run `yarn start:dev`

## Features
- [Openapi validation](https://www.npmjs.com/package/express-openapi#argsvalidateapidoc) (enabled by default)
- Request validation by using [Jsonschema](https://www.npmjs.com/package/jsonschema)
  - Validate body
    ```
    {
      paths: {
        '/pets/createOne': {
          post: {
            requestBody: {
              required: true,
              content: {
                'application/json': {
                  schema: {
                    $ref: '#/components/schemas/CreateOnePetRequest',
                  },
                },
              },
            },
            responses: {
            },
          },
        },
      },
      components: {
        schemas: {
          CreateOnePetRequest: {
            type: 'object',
            properties: {
              name: {
                type: 'string',
                minLength: 5,
              },
            },
            required: ['name'],
          },
        },
      },
    }
    ```
  - Validate params (it does not need the `required` property)
    ```
    '/pets/findOne/{id}': {
      get: {
        parameters: [
          {
            name: 'id',
            in: 'path',
            schema: {
              type: 'string',
              minLength: 5,
            },
          },
        ],
        responses: {
        },
      },
    },
    ```
  - Validate query
    ```
    '/pets/findAll': {
      get: {
        security: [
          {
            oAuth2: ['read'],
          }
        ],
        parameters: [
          {
            name: 'page',
            in: 'query',
            schema: {
              type: 'integer',
              minimum: 0,
            },
            required: true,
          },
          {
            name: 'pageSize',
            in: 'query',
            schema: {
              type: 'integer',
              minimum: 0,
            },
            required: true,
          },
        ],
      },
    },
    ```
- Response validation by using [Jsonschema](https://www.npmjs.com/package/jsonschema) (this is not working)
- Route organization based on `paths` property, for instance
  - Openapi spec
    ```
    paths: {
      '/pets/findAll': {
      },
      '/pets/createOne': {
      },
      '/pets/findOne/{id}': {
      },
    },
    ```
  - Result
    ```
    .
    └── routes
        └── pets
            ├── createOne.js
            ├── findAll.js
            └── findOne
                └── {id}.js
    ```
  - Authorization and authentication using [Security Handler](https://github.com/kogosoftwarellc/open-api/tree/main/packages/openapi-security-handler)