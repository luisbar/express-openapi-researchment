module.exports = {
  openapi: '3.0.0',
  info: {
    title: 'Express Openapi Researchment',
    version: '1.0.0',
  },
  servers: [{ url: '/v1' }],
  paths: {
    '/pets/findAll': {
      get: {
        security: [
          {
            oAuth2: ['read'],
          }
        ],
        parameters: [
          {
            name: 'page',
            in: 'query',
            schema: {
              type: 'integer',
              minimum: 0,
            },
            required: true,
          },
          {
            name: 'pageSize',
            in: 'query',
            schema: {
              type: 'integer',
              minimum: 0,
            },
            required: true,
          },
        ],
        responses: {
          '200': {
            description: 'Return all pets',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/FindAllPetsResponse',
                },
              },
            },
          },
        },
      },
    },
    '/pets/createOne': {
      post: {
        security: [
          {
            oAuth2: ['write'],
          }
        ],
        requestBody: {
          required: true,
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/CreateOnePetRequest',
              },
            },
          },
        },
        responses: {
          '200': {
            description: 'Create and return the created pet',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/FindOnePetResponse',
                },
              },
            },
          },
        },
      },
    },
    '/pets/findOne/{id}': {
      get: {
        security: [
          {
            oAuth2: ['read'],
          }
        ],
        parameters: [
          {
            name: 'id',
            in: 'path',
            schema: {
              type: 'string',
              minLength: 5,
            },
          },
        ],
        responses: {
          '200': {
            description: 'Return a pet',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/FindOnePetResponse',
                },
              },
            },
          },
        },
      },
    },
    '/auth/token': {
      post: {
        requestBody: {
          required: true,
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/TokenRequest',
              },
            },
          },
        },
        responses: {
          '200': {
            description: 'Return an access token',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/TokenResponse',
                },
              },
            },
          },
        },
      },
    },
  },
  components: {
    schemas: {
      FindAllPetsResponse: {
        type: 'array',
        items: {
          $ref: '#/components/schemas/Pet',
        },
      },
      FindOnePetResponse: {
        $ref: '#/components/schemas/Pet',
      },
      CreateOnePetRequest: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            minLength: 5,
          },
        },
        required: ['name'],
      },
      Pet: {
        type: 'object',
        properties: {
          id: {
            type: 'string',
          },
          name: {
            type: 'string',
          },
        },
        required: ['id', 'name'],
      },
      TokenRequest: {
        type: 'object',
        properties: {
          clientId: {
            type: 'string',
          },
          clientSecret: {
            type: 'string',
          },
        },
        required: ['clientId', 'clientSecret'],
      },
      TokenResponse: {
        type: 'object',
        properties: {
          accessToken: {
            type: 'string',
          },
        },
        required: ['accessToken'],
      },
    },
    securitySchemes: {
      oAuth2: {
        type: 'oauth2',
      },
    },
  },
};