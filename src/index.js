const express = require('express')
const { initialize } = require('express-openapi')
const apiDoc = require('./apiDoc')
const path = require('path')
const jsonwebtoken = require('jsonwebtoken')

const app = express()

initialize({
  app,
  apiDoc,
  paths: path.join(__dirname, 'routes'),
  errorMiddleware: (error, req, res, next) => {
    res.status(error.status).json({ errors: error.errors, message: error.message })
  },
  docsPath: '/doc',
  consumesMiddleware: {
    'application/json': express.json(),
  },
  securityHandlers: {
    oAuth2: async (req, requiredScopes, securityDefinition) => {
      const accessToken = req?.headers?.authorization?.split(' ').pop()
      if (!accessToken)
        throw {
          status: 401,
          message: 'Unauthorized',
        }

      const payload = jsonwebtoken.verify(accessToken, process.env.JWT_SECRET)
      if (!payload)
        throw {
          status: 401,
          message: 'Unauthorized',
        }

      const tokenScopes = payload.scopes
      if (!requiredScopes.every(requiredScope => tokenScopes.includes(requiredScope)))
        throw {
          status: 401,
          message: 'Unauthorized',
        }

      return true
    },
  }
})

app.listen(3000, () => console.log('Listening on port 3000'))