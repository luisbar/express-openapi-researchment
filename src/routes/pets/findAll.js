function get(req, res) {
  res.json(global.pets.slice(req.query.page * req.query.pageSize, req.query.page * req.query.pageSize + req.query.pageSize))
}

module.exports = { get }