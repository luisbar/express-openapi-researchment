function get(req, res) {
  res.json(global.pets.find(pet => pet.id === req.params.id))
}

module.exports = { get }