const crypto = require('crypto')
global.pets = []

function post(req, res) {
  const newPet = {
    id: crypto.randomUUID(),
    name: req.body.name
  }
  global.pets.push(newPet)

  res.json({ ...newPet })
}

module.exports = { post }