const jsonwebtoken = require('jsonwebtoken');

function post(req, res) {
  if (!req.body.clientId || !req.body.clientSecret)
    return res.status(400).json({ message: 'clientId and clientSecret are required' });

  res.json({
    accessToken: jsonwebtoken.sign(
      {
        scopes: ['read', 'write'],
      },
      process.env.JWT_SECRET
    )
  })
}

module.exports = { post }